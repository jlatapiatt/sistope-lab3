CC = gcc

EXEC1 = secuencial
EXEC2 = aleatorio

ODIR = ./obj
SRC = ./src

_OBJ1 = secuencial.o
OBJ1 = $(ODIR)/$(_OBJ1)

_OBJ2 = aleatorio.o
OBJ2 = $(ODIR)/$(_OBJ2)

all: $(EXEC1) $(EXEC2)

$(ODIR)/%.o: $(SRC)/%.c
	$(CC) -c -o $@ $^

$(EXEC1): $(OBJ1)
	$(CC) -o $@ $^

$(EXEC2): $(OBJ2)
	$(CC) -o $@ $^

.PHONY: clean

#clean:
	#rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 