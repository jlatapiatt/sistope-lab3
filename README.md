# Laboratorio numero 3 para la asignatura Sistemas Operativos README


En este git, se encuentran los códigos para dos programas, que se encuentran en la carpeta /src. Los nombres de estos son aleatorio.c y secuencial.c


El programa aleatorio.c lo que hace es crear un arreglo de enteros, después en un ciclo con iteraciones igual a la cantidad de posiciones en el arreglo, genera un número aleatorio, y en la posición igual a este numero, guarda en la posición el número obtenido.


El programa secuencial.c igualmente tiene un arreglo de enteros, pero este recorre secuencialmente el arreglo, guardando un número aleatorio en cada posición del arreglo.


### Ejecución de los programas:


Lo primero que debe hacer para poder ejecutar ambos programas es compilarlos. Para ello puede ocupar el archivo makefile facilitado en este git. Basta usar el comando make, para que genere los ejecutables aleatorio y secuencial.


Si es que desea medir el tiempo de una ejecución de uno de los programas puede usar la función time de linux, de la siguiente forma:




```
time ./{nombreEjecutable}


```


Para ejecutarlo en repetidas ocasiones, y obteniendo el tiempo de ejecución de un programa puede usar la siguiente línea de comando:




```
seq N | xargs -Iz time ./{nombreEjecutable}


```


Donde N es la cantidad de veces que desee ejecutar el programa.
