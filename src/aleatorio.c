#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define ARRAY_SIZE 536870912
//#define inverse(r) (int)()

int randomGenerator (int max_Number){
  long inverse = RAND_MAX / (max_Number+1);
  int x = rand();
  while (x >= max_Number / inverse){
    x = rand();
  }
  return x;
}


int main () {
  int *rngod = (int*) malloc(sizeof(int)*ARRAY_SIZE);
  int aux;
  srand(time(NULL));
  for (int i = 0; i < (ARRAY_SIZE-1); i++){
    aux = randomGenerator(ARRAY_SIZE);
    rngod[aux] = aux;
  }

  return 0;
}
